(function() {
    $(window).on('resize', function() {
        let heightGet = parseInt($('.alertBox').children('div:nth-child(2)').css('height'))/2 - 14;
        console.log(heightGet);
        $('.alertBox').children('div:nth-child(1)').css({
            padding: heightGet + 'px 2rem'
        });


        let registrationContainerWidth = $('.registrationStep .container').css('width');
        $('#StepCounter').css('width', registrationContainerWidth).css('margin', '3rem auto');
    });
    $(window).on('load', function() {
        let heightGet = parseInt($('.alertBox').children('div:nth-child(2)').css('height'))/2 - 14;
        console.log(heightGet);
        $('.alertBox').children('div:nth-child(1)').css({
            padding: heightGet + 'px 2rem'
        });
        let registrationContainerWidth = $('.registrationStep .container').css('width');
        $('#StepCounter').css('width', registrationContainerWidth).css('margin', '3rem auto');
    });
    $('#DateOfBirth').datepicker({
        dateFormat: "mm/dd/y",
        showOn: "focus"
    });
    $('#CalendarIcon').on('click', function() {
        $('#DateOfBirth').focus();
    });

    $('.popoverParent').on('focus', function() {
        $(this).popover('show');
    });
})();